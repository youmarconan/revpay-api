package com.revature.revpay.model;

import lombok.Data;

@Data
public class RequestView {
    private int id;
    private String requestSender;
    private double amount;
    private String status;


    public RequestView(Request r) {
        this.id = r.getId();
        this.requestSender = r.getRequestSender().getFirstName();;
        this.amount = r.getAmount();
        this.status = r.getStatus();
    }

    
}

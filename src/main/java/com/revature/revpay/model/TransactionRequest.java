package com.revature.revpay.model;

import lombok.AllArgsConstructor;
import lombok.Data;
import lombok.NoArgsConstructor;

@Data
@AllArgsConstructor
@NoArgsConstructor
public class TransactionRequest {
    
    private String moneySenderPhoneNumber;

    private String by;

    private String moneyReceiver;

    private double amount;
}

package com.revature.revpay.model;

import lombok.AllArgsConstructor;
import lombok.Data;
import lombok.NoArgsConstructor;

@Data
@AllArgsConstructor
@NoArgsConstructor
public class RequestRequest {
    
    private String requestSenderPhoneNumber;

    private String by;

    private String requestReceiver;

    private double amount;

}

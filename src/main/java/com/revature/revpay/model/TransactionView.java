package com.revature.revpay.model;

import lombok.Data;

@Data
public class TransactionView {
    
    private int id;
    private String sender;
    private String receiver;
    private String time;
    private double amount;

    public TransactionView(Transaction t) {
        this.id = t.getId();
        this.sender = t.getSender().getFirstName();
        this.receiver = t.getReceiver().getFirstName();
        this.time = t.getTime();
        this.amount = t.getAmount();
    }

    
}

package com.revature.revpay.common.exceptions;

public class IsAlreadyExist extends RuntimeException  {

    public IsAlreadyExist(String field) {
        super("The provided " + field + " already exsits!");
    }
    
}

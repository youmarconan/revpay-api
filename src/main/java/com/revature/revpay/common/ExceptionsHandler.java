package com.revature.revpay.common;



import org.springframework.http.HttpStatus;
import org.springframework.web.bind.annotation.ExceptionHandler;
import org.springframework.web.bind.annotation.ResponseStatus;
import org.springframework.web.bind.annotation.RestControllerAdvice;

import com.revature.revpay.common.exceptions.AuthenticationException;
import com.revature.revpay.common.exceptions.DataSourceException;
import com.revature.revpay.common.exceptions.InvalidRequestException;
import com.revature.revpay.common.exceptions.IsAlreadyExist;

@RestControllerAdvice
public class ExceptionsHandler {

    @ExceptionHandler({AuthenticationException.class})
    @ResponseStatus(HttpStatus.UNAUTHORIZED)
    public Error handleAuthenticationExceptions(AuthenticationException e) {
        return new Error(401, e.getMessage());
    }
    
    @ExceptionHandler({InvalidRequestException.class})
    @ResponseStatus(HttpStatus.BAD_REQUEST)
    public Error handleBadRequests(Exception e) {
        return new Error(400, e.getMessage());
    }

    @ExceptionHandler({IsAlreadyExist.class})
    @ResponseStatus(HttpStatus.CONFLICT)
    public Error handleIsAlreadyExistException(IsAlreadyExist e) {
        return new Error(409, e.getMessage());
    }

    @ExceptionHandler({DataSourceException.class})
    @ResponseStatus(HttpStatus.SERVICE_UNAVAILABLE)
    public Error handleDataSourceExceptions(DataSourceException e) {
        return new Error(502, e.getMessage());
    }

    @ExceptionHandler
    @ResponseStatus(HttpStatus.INTERNAL_SERVER_ERROR)
    public Error handleOtherExceptions(Exception e) {
        //e.printStackTrace();
        return new Error(500, "An unexpected exception occurred. Devs, please check logs."+ e.getMessage());
    }


}

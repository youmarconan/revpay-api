package com.revature.revpay.controller;


import java.util.List;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.CrossOrigin;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.PostMapping;
import org.springframework.web.bind.annotation.PutMapping;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RestController;

import com.revature.revpay.common.exceptions.InvalidRequestException;
import com.revature.revpay.common.exceptions.IsAlreadyExist;
import com.revature.revpay.model.Credentials;
import com.revature.revpay.model.Request;
import com.revature.revpay.model.RequestRequest;
import com.revature.revpay.model.RequestView;
import com.revature.revpay.model.Transaction;
import com.revature.revpay.model.TransactionRequest;
import com.revature.revpay.model.TransactionView;
import com.revature.revpay.model.User;
import com.revature.revpay.service.userService;

@RestController
@RequestMapping("/user")
@CrossOrigin(origins="http://localhost:4200/", allowCredentials="true")
public class UserController {
    private userService userService;

    @Autowired
    public UserController(com.revature.revpay.service.userService userService) {
        this.userService = userService;
    }

    @GetMapping("/admin/user")
    public ResponseEntity<List<User>> allUsers(){
        
        return ResponseEntity.ok(userService.allUsers());
    }

    @GetMapping("/admin/transaction")
    public ResponseEntity<List<Transaction>> allTransactions(){
        
        return ResponseEntity.ok(userService.allTransactions());
    }

    @GetMapping("/admin/request")
    public ResponseEntity<List<Request>> allrequests(){
        
        return ResponseEntity.ok(userService.allRequests());
    }

    @PostMapping("/login")
    public ResponseEntity<User> login (@RequestBody Credentials credentials) throws InvalidRequestException{
        return ResponseEntity.ok(userService.login(credentials.getUsername(),credentials.getPassword()));
    }

    @PostMapping("/signup")
    public ResponseEntity<User> signup (@RequestBody User user) throws InvalidRequestException, IsAlreadyExist {
        return ResponseEntity.ok(userService.register(user));
    }

    @GetMapping("/transactions/history/{number}")
    public ResponseEntity<List<TransactionView>> allTransacctions (@PathVariable String number) throws InvalidRequestException{
        return ResponseEntity.ok(userService.viewTransactionsHistory(number));
    }

    @PutMapping("/transactions/sendmoney")
    public void sendMoney(@RequestBody TransactionRequest transactionRequest) throws InvalidRequestException{
        userService.sendMoney(transactionRequest);
    }

    @PostMapping("/request/send")
    public void sendMoneyRequest(@RequestBody RequestRequest requestRequest){
        userService.requestMoney(requestRequest);
    }

    @GetMapping("/request/received/{number}")
    public ResponseEntity<List<RequestView>> allRequests(@PathVariable String number){
        
        return ResponseEntity.ok(userService.viewReceivedRequests(number));
    }

    
    
}

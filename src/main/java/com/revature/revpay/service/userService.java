package com.revature.revpay.service;

import java.time.LocalDateTime;
import java.util.List;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;
import org.springframework.transaction.annotation.Transactional;

import com.revature.revpay.common.exceptions.AuthenticationException;
import com.revature.revpay.common.exceptions.InvalidRequestException;
import com.revature.revpay.common.exceptions.IsAlreadyExist;
import com.revature.revpay.model.Request;
import com.revature.revpay.model.RequestRequest;
import com.revature.revpay.model.RequestView;
import com.revature.revpay.model.Transaction;
import com.revature.revpay.model.TransactionRequest;
import com.revature.revpay.model.TransactionView;
import com.revature.revpay.model.User;
import com.revature.revpay.repo.RequestRepo;
import com.revature.revpay.repo.TransactionRepo;
import com.revature.revpay.repo.UserRepo;

@Service
public class userService {
    private UserRepo userRepo;
    private TransactionRepo transactionRepo;
    private RequestRepo requestRepo;

    @Autowired
    public userService(UserRepo userRepo, TransactionRepo transactionRepo, RequestRepo requestRepo) {
        this.userRepo = userRepo;
        this.transactionRepo = transactionRepo;
        this.requestRepo = requestRepo;
    }

    public List<User> allUsers(){
        return userRepo.findAll();
    }

    public List<Transaction> allTransactions(){
        return transactionRepo.findAll();
    }

    public List<Request> allRequests(){
        return requestRepo.findAll();
    }

    public User login (String username, String password) throws InvalidRequestException{

        if( password == null){
            throw new InvalidRequestException("Must provide valid PASSWORD");
        }

        return userRepo.findUserByUsernameAndPassword(username, password).orElseThrow(AuthenticationException::new);
    }

    public User register(User user) throws InvalidRequestException,IsAlreadyExist{

        if(user.getFirstName().trim() == "" || user.getLastName().trim() == "" || user.getFirstName() == null || user.getLastName() == null){
            throw new InvalidRequestException("Must provide FIRST NAME and LAST NAME");
        }

        if(user.getUsername().trim() == "" || user.getUsername() == null){
            throw new InvalidRequestException("Must provide UNIQUE USERNAME");
        }

        if(user.getEmail().trim() == "" || user.getEmail() == null){
            throw new InvalidRequestException("Must provide EMAIL");
        }

        if(user.getPhoneNumber().trim() == "" || user.getPhoneNumber() == null){
            throw new InvalidRequestException("Must provide PHONE NUMBER");
        }

        if(user.getPassword().trim() == "" || user.getPassword() == null){
            throw new InvalidRequestException("Must provide valid PASSWORD");
        }

        if(userRepo.existsByEmail(user.getEmail())){
            throw new IsAlreadyExist("EMAIL");
        }

        if(userRepo.existsByUsername(user.getUsername())){
            throw new IsAlreadyExist("USERNAME");
        }

        if(userRepo.existsByPhoneNumber(user.getPhoneNumber())){
            throw new IsAlreadyExist("PHONE NUMBER");
        }

        return userRepo.save(user);
    }

    public List<TransactionView> viewTransactionsHistory(String number){

        User user = userRepo.findByPhoneNumber(number).orElseThrow(InvalidRequestException::new);
        
        return transactionRepo.findTransactionBySenderOrReceiver(user,user).stream().map(t -> t.getTransactionView(t)).toList();
    }

    @Transactional
    public void sendMoney(TransactionRequest transactionRequest) throws InvalidRequestException{

        User sender = userRepo.findByPhoneNumber(transactionRequest.getMoneySenderPhoneNumber()).orElseThrow(InvalidRequestException::new);

        if(transactionRequest.getAmount() > sender.getCurrentBalance()){
            throw new InvalidRequestException("The amount you're tring to send is greater than your current balance");
        }

        Transaction transaction = new Transaction();

        transaction.setAmount(transactionRequest.getAmount());

        transaction.setSender(sender);

        sender.setCurrentBalance(sender.getCurrentBalance() - transaction.getAmount());

        if(transactionRequest.getBy().equalsIgnoreCase("email")){
            User receiver = userRepo.findByEmail(transactionRequest.getMoneyReceiver()).orElseThrow(InvalidRequestException::new);
            receiver.setCurrentBalance(receiver.getCurrentBalance() + transaction.getAmount());
            transaction.setReceiver(receiver);
        }else if(transactionRequest.getBy().equalsIgnoreCase("number")){
            User receiver = userRepo.findByPhoneNumber(transactionRequest.getMoneyReceiver()).orElseThrow(InvalidRequestException::new);
            receiver.setCurrentBalance(receiver.getCurrentBalance() + transaction.getAmount());
            transaction.setReceiver(receiver);
        }else if(transactionRequest.getBy().equalsIgnoreCase("username")){
            User receiver = userRepo.findByUsername(transactionRequest.getMoneyReceiver()).orElseThrow(InvalidRequestException::new);
            receiver.setCurrentBalance(receiver.getCurrentBalance() + transaction.getAmount());
            transaction.setReceiver(receiver);
        }

                

        transaction.setTime(LocalDateTime.now().toString());

        transactionRepo.save(transaction);

    }

    public List<RequestView> viewReceivedRequests(String phoneNumber){
        User user = userRepo.findByPhoneNumber(phoneNumber).orElseThrow(InvalidRequestException::new);
        return requestRepo.findByRequestReceiver(user).stream().map(r -> r.getRequestView(r)).toList();
    }

    @Transactional
    public void requestMoney(RequestRequest requestRequest){

        User requestSender = userRepo.findByPhoneNumber(requestRequest.getRequestSenderPhoneNumber()).orElseThrow(InvalidRequestException::new);

        Request request = new Request();

        request.setRequestSender(requestSender);

        if(requestRequest.getBy().equalsIgnoreCase("email")){
            User requestReceiver = userRepo.findByEmail(requestRequest.getRequestReceiver()).orElseThrow(InvalidRequestException::new);
            request.setRequestReceiver(requestReceiver);
        }else if(requestRequest.getBy().equalsIgnoreCase("number")){
            User requestReceiver = userRepo.findByPhoneNumber(requestRequest.getRequestReceiver()).orElseThrow(InvalidRequestException::new);
            request.setRequestReceiver(requestReceiver);
        }else if(requestRequest.getBy().equalsIgnoreCase("username")){
            User requestReceiver = userRepo.findByUsername(requestRequest.getRequestReceiver()).orElseThrow(InvalidRequestException::new);
            request.setRequestReceiver(requestReceiver);
        }

        request.setAmount(requestRequest.getAmount());        

        request.setStatus("pending");

        requestRepo.save(request);
    }





    
}

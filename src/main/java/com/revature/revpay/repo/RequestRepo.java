package com.revature.revpay.repo;

import java.util.List;

import org.springframework.data.jpa.repository.JpaRepository;

import com.revature.revpay.model.Request;
import com.revature.revpay.model.User;

public interface RequestRepo extends JpaRepository<Request,Integer> {
    List<Request> findByRequestReceiver(User requestReceiver);
}

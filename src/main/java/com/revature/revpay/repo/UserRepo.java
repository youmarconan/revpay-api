package com.revature.revpay.repo;

import java.util.Optional;

import org.springframework.data.jpa.repository.JpaRepository;
import org.springframework.stereotype.Repository;

import com.revature.revpay.model.User;

@Repository
public interface UserRepo extends JpaRepository<User,Integer>{

    Optional<User> findUserByUsernameAndPassword(String username, String password);

    public boolean existsByEmail(String email);

    public boolean existsByUsername(String username);

    public boolean existsByPhoneNumber(String phoneNmuber);

    Optional<User> findByEmail(String email);

    Optional<User> findByUsername(String username);

    Optional<User> findByPhoneNumber(String phoneNmuber);

}

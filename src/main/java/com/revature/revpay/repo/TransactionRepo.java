package com.revature.revpay.repo;

import java.util.List;

import org.springframework.data.jpa.repository.JpaRepository;

import com.revature.revpay.model.Transaction;
import com.revature.revpay.model.User;

public interface TransactionRepo extends JpaRepository<Transaction,Integer> {
    
    List<Transaction> findTransactionBySenderOrReceiver(User sender, User receiver);
    
}

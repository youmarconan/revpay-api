package com.revature.revpay;

import org.springframework.boot.SpringApplication;
import org.springframework.boot.autoconfigure.SpringBootApplication;

@SpringBootApplication
public class RevpayApplication {

	public static void main(String[] args) {
		SpringApplication.run(RevpayApplication.class, args);
	}

}
